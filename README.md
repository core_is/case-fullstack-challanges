<p align="center">
<img style="border:0" src="https://ci6.googleusercontent.com/proxy/WS4kfwJSbGsxD2e3BETfCMs4ji_XHpsAez6vORHdKyGgj-JH5357cXOtpoUZQ5RYKQd4g09TWihmE-JNKxWU8wmOpZ0wHrOeZuOY81eGXPUqm0fIDB7Z7KZjXSP3OOs=s0-d-e1-ft#https://d335luupugsy2.cloudfront.net/cms/files/269477/1634907888/$4yeby9jrx5" >
</p>

> **Termo de confidencialidade**
>
> As imagens, gráficos e informações contidas neste documento constituem propriedade intelectual da
> I.Systems, sendo resultados diretos de pesquisas e esforços de desenvolvimento privado, desta forma,
> elas não podem ser transmitidas ou reproduzidas inteiramente ou parcialmente sem autorização prévia
> por escrito.

# Case técnico - Full Stack

**Estamos mais do que felizes em ver que você se interessa pelo nosso desafio!**

Esta etapa é feita para conhecermos um pouco mais de cada pessoa candidata ao cargo de Full Stack na I.Systems. Não se trata de uma prova objetiva, capaz de gerar uma nota ou uma taxa de sucesso, mas sim um estudo de caso com a finalidade de explorar os seus conhecimentos, experiências e forma de trabalhar. Sinta-se à vontade para desenvolver sua solução para o problema proposto. Você pode nos enviar suas dúvidas: não há limite de perguntas e nos comprometemos a enviar uma resposta em até 24h, utilize essa liberdade para validar suas ideias e não ficar “travado” durante a resolução do case.

[Veja todas as nossas vagas em aberto](isystems.gupy.io)

## Desafio

---

### Front-end

![layout](./assets/layout-front.png)

- Você pode usar as bibliotecas ou frameworks que preferir
- Todos os campos do formulário são obrigatórios
- Ficaremos felizes se você escrever testes

### Back-end

O front tem um formulário para enviar os campos `First name`, `Last name` e `Participation`. E um gráfico de pizza que exibe esses dados.

- Você precisa criar APIs para enviar e receber essas informações
- No caso de inconsistência, retorne o erro em um JSON estruturado com o código HTTP 400
- Ficaremos felizes se você escrer testes

### AWS

Também queremos avaliar seus conhecimentos nos serviços AWS, fique a vontade para propor soluções para o desafio. Por exemplo:

- Indique como você serviria o front
- Indique como você serviria o back em algum serviço aws

### Desenvolvimento
Clone este repositório em sua máquina de desenvolvimento. Faça commits relevantes. Vamos analisar as mensagens usadas nos commits assim como o conteúdo de cada commit. A idéia é que cada commit seja tratado como se fosse um Pull Request (PR). Os commits não podem ser muito grandes e nem ter código não relacionado com a mensagem.

Para submeter sua solução, compacte a pasta raiz e nos envie por e-mail. Caso prefira, você pode subir sua solução em um repositório privado e convidar o avaliador para visualizar (neste caso, nos pergunte qual o endereço de e-mail do avaliador). Não exponha publicamente sua solução na internet.


#### Plus

Sinta-se a vontade para realizar melhorias.
